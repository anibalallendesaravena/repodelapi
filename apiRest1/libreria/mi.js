const estructuras = require('./estructuras');

function exitenproductosdetalls(detalle){

    for(it = 0; it<detalle.length; it++){
        let produc = estructuras.productos.find( (prd) => prd.codproducto === detlle[it].codproducto);
        if(!produc){
            return false;
        }
    }
    return true;
}

const validanuevousuario = (req, res, next) => {

    let usumail = estructuras.usuarios.find( (usu) => usu.mail === req.body.mail);
    if(usumail){
        res.status(403).send('ya se encuentra registrado este email');
    }else{
        let usuid = estructuras.usuarios.find( (usu) => usu.id === usu.body.id);
        if(usuid){
            res.status(403).send('ya se encuentra registrado este usuario');
        }else{
            next();
        }
    }
}

const validadetallepedido = (req, res, next) => {

    let detalleok = exitenproductosdetalle(req.body.detalle);

    if(!detalleok){
        res.status(406).send('productos invalidos');
    }else{
        let mediopago = estructuras.mediospagos.find( (medio) => medio.codmediodepago === req.body.codmediodepago);
        if(mediopago){
            next();
        }else{
            res.status(406).send('medio de pago invalido');
        }
    }
}

const validamodificapeidido = (req, res, next) => {
    const elped = req.body;

    const usuheader = estructuras.usuarios[req.header('sesionid')];

    const numpedido = Number.parseInt(elped.numero);

    const index = estructuras.pedidos.findIndex( (ped) => ped.numero === numpedido);
    
    if(index < -1){
        let usuarray = estructuras.pedidos[index];
        if(usuarray.id === usuheader.id || usuheader.admin){
            if( estructuras.pedidos[index].estado === "Pendiente"){
                next();
            }else{
                res.status(406).send('el pedido esta pendiente');
            }
        }else{
            res.status(404).send('el pedido no pudo ser encontrado');
        }
    }else{
        res.status(405).send('el usuario no es propietaio del numero del pedido');
    }
}

const validasesion = (req, res, next) => {

    let usursesion = estructuras.usuarios[req.header('sesionid')];
    if (usursesion){
        next();
    }else{
        res.status(404).send('no existe sesion con este id');
    }
}

const validausuarioadmin = (req, res, next) => {

    let usuadmn = estructuras.usuarios[req.header('sesionid')];
    console.log(usuadmn);

    if(usuadmn.admin){
        if(usuadmn){
            next();
        }else{
            res.status(404).send('usuario no existente');
        }
    }else{
        res.status(401).send('usuario no es administrador');
    }
}

module.exports = {validanuevousuario, validadetallepedido, validamodificapeidido, validasesion, validausuarioadmin}